<?php

return [
    'notificationEmails' => getenv('NOTIFICATION_EMAILS', ''),
    'fromEmail' => getenv('FROM_EMAIL')
];
