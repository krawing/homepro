const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const dev_server_host = process.env.DEV_SERVER_HOST || 'localhost';
const dev_server_port = process.env.DEV_SERVER_PORT || 8001;
const devMode = process.env.NODE_ENV !== 'production'

module.exports = {
    entry: ['./src/app.js'],
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: '/dist'
    },
    optimization: {
        minimize: false,
        runtimeChunk: {
            name: 'vendor'
        },
        splitChunks: {
            cacheGroups: {
                default: false,
                commons: {
                    test: /node_modules/,
                    name: "vendor",
                    chunks: "initial",
                    minSize: 1
                }
            }
        }
    },
    plugins: [
        new BundleTracker({filename: './webpack-stats.json'}),
        new MiniCssExtractPlugin({
          // Options similar to the same options in webpackOptions.output
          // both options are optional
          filename: devMode ? '[name].css' : '[name].[hash].css',
          chunkFilename: devMode ? '[id].css' : '[id].[hash].css',
        }),
        new webpack.ProvidePlugin({
              $: 'jquery',
              jQuery: 'jquery',
              'window.jQuery': 'jquery'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.SourceMapDevToolPlugin({
          filename: "[file].map"
        })
    ],
    module: {
        rules: [
            {
                test: /\.js$/, // Check for all js files
                exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
                loader: 'babel-loader'
            },
            {
                test: /\.(scss|sass|css)$/i,
                use: [
                    { loader: MiniCssExtractPlugin.loader, options: {sourceMap: true} },
                    { loader: 'css-loader', options: { minimize: !devMode } },
                    { loader: 'sass-loader', options: { sourceMap: true } },
                    { loader: 'resolve-url-loader' }
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            },
            {
              test: /\.(gif|png|jpe?g|svg)$/i,
              use: [
                'file-loader',
                {
                  loader: 'image-webpack-loader',
                  options: {
                    bypassOnDebug: true, // webpack@1.x
                    disable: true, // webpack@2.x and newer
                  },
                },
              ],
            }
        ]
    },
    resolve: {
        modules: [
            "node_modules"
        ],
    },
    performance: {
        hints: false
    },
    devtool: 'source-map',
    devServer: {
        historyApiFallback: true,
        noInfo: true,
        hot: true,
        disableHostCheck: true,
        host: dev_server_host,
        port: dev_server_port
    }
}
