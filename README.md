# Homepro

Homepro langing

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and 
testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

PHP 5.4+
composer
sendmail

### Installing

Install all required by project PHP dependencies with

```
composer install
```

Copy .env.example to .env and configure NOTIFICATION_EMAILS and FROM_EMAIL