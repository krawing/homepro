<?php
require_once 'config/bootstrap.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

session_start();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $response = [];
    $name = isset($_POST['name']) ? $_POST['name'] : null;
    $email = isset($_POST['email']) ? $_POST['email'] : null;
    $message = isset($_POST['message']) ? $_POST['message'] : null;
    $captchaPhrase = isset($_POST['captcha']) ? $_POST['captcha'] : '';

    // Код с капчи неправильный
    if ($captchaPhrase != $_SESSION['captcha']) {
        $response['result'] = 'error';
        $response['error'] = 'Код с картинки введён неправильно';
    }
    // Поле не видимо ни для кого кроме ботов
    elseif ($message && $message != '') {
        $response['result'] = 'error';
        $response['error'] = '';
    }
    elseif (!$name) {
        $response['result'] = 'error';
        $response['error'] = 'Имя не указано';
    }
    elseif (!$email) {
        $response['result'] = 'error';
        $response['error'] = 'Email не указан';
    }
    else {
        $mail = new PHPMailer;
        $mail->IsHTML(true);
        $mail->setFrom($config['fromEmail']);
        $mail->CharSet = 'utf-8';
        $mail->Subject  = 'Заказ проекта с сайта';
        $mail->Body     = 'Был оформлен заказ с сайта. <br/>
        <b>Имя: </b>' . $name . '<br/>
        <b>Email: </b>' . $email . '<br/>';

        $notificationEmails = explode(',', $config['notificationEmails']);

        foreach ($notificationEmails as $notificationEmail) {
            $mail->addAddress($notificationEmail);
        }

        // Не удалось отправить письмо
        if(!$mail->send()) {
            $response['result'] = 'error';
            $response['error'] = 'Не удалось отправить письмо. Пожалуйста повторите попытку позже';
            $response['trace'] = $mail->ErrorInfo;
        } else {
            $response['result'] = 'success';
        }
    }

    echo json_encode($response);
}
