<?php
require_once 'config/bootstrap.php';

use Gregwar\Captcha\CaptchaBuilder;
session_start();
header('Content-type: image/jpeg');

$builder = new CaptchaBuilder;
$builder->build();
$builder->output();

// Сохранить в сессию
$_SESSION['captcha'] = $builder->getPhrase();
