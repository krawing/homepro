import $ from 'jquery';
import 'jquery.mmenu';
import Swiper from 'swiper';
import 'fotorama/fotorama.js';
import './styles/main.scss';
import * as Cookies from "js-cookie";

$(function() {
    var current_language = Cookies.get('language');

    var $menu = $("#my-menu").mmenu(
      {
        onClick:{
        close:true,
      },
      'extensions': [
        'position-front',
         'position-right'
        ]  
       }
   );
    var $icon = $("#mmenu-icon");
    var API = $menu.data( "mmenu" );
    var fotorama = null;
    var swiper = null;

    function showGallery(e)
    {
        e.preventDefault();

        var data = [];
        var project_images = $(this)
          .parent('.project__item')
          .find('.project__images img');

        project_images.each(function() {
            var src = $(this).attr('src');
            data.push({image: src, thumb: src});
        });

        jQuery('.popup__overlay').fadeIn(400, function() {
            // Фоторама не инициализируется на скрытом объекте, поэтому ждём пока покажется попап
            if (!fotorama) {
                $('.popup .popup__gallery').fotorama({
                    width: '80%',
                    maxwidth: '80%',
                    allowfullscreen: false,
                    nav: 'thumbs',
                    fit: 'scaledown'
                });

                fotorama = $('.popup .popup__gallery').data('fotorama');
            }

            // Загружаем слайды
            fotorama.load(data);
            fotorama.show(0);
        });
    }

    function showVideo(e) {
        e.preventDefault();

        var videoUrl = $(this).attr('href');
        var screenWidth = window.innerWidth;

        var popupHtml = `<iframe class="popup__video" src="${videoUrl}?autoplay=true" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`;

        if (fotorama) {
            fotorama.destroy();
        }

        $('.popup').addClass('video').find('.popup__gallery').html(popupHtml);

        jQuery('.popup__overlay').fadeIn(400);
    }

    $icon.on( "click", function() {
        API.open();
    });

    function switchLanguage(language) {
      $(':not(html)[lang]').not('[lang="' + language + '"]').hide();
      $(':not(html)[lang="' + language + '"]').show();
      $('.language-switcher div').not('[data-lang=' + language + ']').show();
      $('.language-switcher div[data-lang=' + language + ']').hide();
      $('input[name=email]').removeProp('required');
      $('input[name=email]:lang(' + language + ')').prop('required', true);
      Cookies.set('language', language)
    }


    API.bind( "opened", function() {
       setTimeout(function() {

          $icon.addClass( "is-active" );

       }, 100);

       $icon.on( "click", function() {

          API.close();

         });
    });

    API.bind( "closed", function() {
        setTimeout(function() {
           $icon.removeClass( "is-active" );
        }, 100);

        $icon.on( "click", function() {
           API.open();
        });
    });

    var swiper = new Swiper('.project__wrapper', {
        slidesPerView: 2,
        spaceBetween: 20,
        slidesPerGroup: 2,
        pagination: {
            el: '.project__nav',
            clickable: true,
            renderBullet: function (index, className) {
                return '<li class="' + className + ' project__bullet"><a class="project__link">' + (index + 1) + '</a></li>';
            },
        },
        breakpointsInverse: true,
        breakpoints: {
            993: {
                slidesPerView: 2
            },
            0: {
                slidesPerView: 1,
                slidesPerGroup: 1
            }
        }
    });

    // Показываем галерею по клику на изображение проекта
    $('#projects .project__item > a').on('click', showGallery);

    // Показываем всплывающее видео по клику на изображение урока
    $('#videos .project__item > a').on('click', showVideo);

    // По клику на оверлей скрываем его и попап
    $('.popup__overlay').on('mouseup', function(e) {
        e.preventDefault();

        if(e.target == this) {

            $(this).fadeOut(400, function() {
                $('.popup').removeClass('video');
                $('.popup .popup__video').remove();
            });
        }
    });

    $('#order form').on('submit', function(e) {
        e.preventDefault();

        var url = $(this).attr('action');
        var data = $(this).find(":input:not(:hidden)").serializeArray();
        
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function(data) {
                var message = '';

                if (data.result == 'error') {
                    message = data.error;
                }
                else {
                    message = 'Сообщение было успешно отправлено';
                }

                $('.form__message:visible span')
                  .removeClass('error success')
                  .addClass(data.result)
                  .html(message)
                  .show()
                  .fadeOut(2000);
            }
        });
    });

    $("[name=message]").hide();

    $(".captcha").on("click", function(e) {
      var image = $(e.target);
      
      image
        .attr("src", "")
        .attr("src", "/captcha.php");
    });

    $('.form__send').on('click', function() {
      var form = $('.contacts__form');

      if (form.find('input[type=submit]').length == 0) {
          form.append('<input type="submit" class="hide">');
      }

      form.find('input[type=submit]').click();
    });

    $('.language-switcher> div').on('click', function() {
      var lang = $(this).attr('data-lang');

      switchLanguage(lang);

      return false;
    });

    if (!current_language) {
        current_language = 'ru-ru';
    }

    switchLanguage(current_language);

    document.getElementById('my-menu').style.opacity=1;

    // Add accordion
    var acc = document.getElementsByClassName("accordion");
    var i;

    var doit= function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.display === "block") {
        panel.style.display = "none";
        } else {
        panel.style.display = "block";
        }
    }
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click",  doit);
        acc[i].addEventListener("touchend",  doit);

    }
    
    var copyDate = document.getElementById("copyDate");
    var nowDate = new Date;
    copyDate.innerHTML = nowDate.getFullYear();
    
});